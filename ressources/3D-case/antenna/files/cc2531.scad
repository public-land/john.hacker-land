difference(){
	import("cc2531_Bottom.stl");
	translate([-10,9.5,2])
		cube([14,7,7]);
}

translate([0,-5,1.4])
difference(){
	import("cc2531_Top.stl");
	translate([-10,-20.5,0.6])
		cube([14,7,7]);
}