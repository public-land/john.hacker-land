# Hacker-land !

Recettes de cuisine pour hacker des objects connectés et plus encore ?!

![The scroll of Truth !](img/the_scroll_of_truth.jpg)

*Je l'ai finalement trouvé... Aprés 15 ans*  
**LE ROULEAU DE LA VÉRITÉ!**  
`Vim, c'est pas si difficile à apprendre`  
*!*

# Setup basique du Raspberry

- Installation le l'OS:  

     - RDV sur [la page officiel](https://www.raspberrypi.org/downloads/raspbian/) de la fondation pour telecharger .zip de l'OS
     **ATTENTION** Par défault, le serveur SSH n'est pas activé !, pour se faire, il suffit de créer un fichier vide nommé `ssh` à la racine de la carte SD. Voir [les explications officielles ici.](https://www.raspberrypi.org/documentation/remote-access/ssh/)  
       
- Connexion SSH par défaut:  
    > login : pi  
    mdp : raspberry  

- Pour configurer son Raspberry-Pi:  
Lancer le programme spécialement conçu pour la config du système  
`$ sudo raspi-config`  

# Projet *zigbee2mqtt*

## Prérequis  

- Dongle USB CC2531
- Raspberry-Pi
- Des objets connectés qui ont ZigBee comme protocole de communication

## Vue d'emsemble:
*Zigbee2mqtt* est un programme en Node.js (JavaScript en dehors du navigateur) qui fonctionne avec le dongle USB qui comporte la puce CC2531. La puce CC2531 est un *sniffer* qui écoute et communique avec tout les objets connectés qui tournent avec le protocole ZigBee.

Le programme *Zigbee2mqtt*, va nous permettre de pratiquer 2 protocoles importants dans le monde de L'**IOT** ( l'Internet des Objects, **Internet Of Things**) et de la domotique:  le protocole de communication **ZigBee** et le protocole de messagerie **mqtt**.

Des explications rapides de ces 2 protocoles:
  
- Le protocole de communication **ZigBee**:  

    Dans la jungle des normes que constitue *la domotique*, le protocole ZigBee permet de faire communiquer des objets dans une rayon interressant (environ 20m), plutôt pas mal pour la maison, avec de la sécurité et tout et tout...  
       
    - Pour plus d'explications sur le net:  
        - [ici, un article de blog explicatif](https://connect.ed-diamond.com/MISC/MISC-086/Tout-tout-tout-vous-saurez-tout-sur-le-ZigBee)  
          
    ```
    Le ZigBee est un protocole de communication sans-fil à courte portée et à faible consommation
    énergétique basé sur la norme IEEE 802.15.4. Il est maintenu par un consortium regroupant des
    entreprises, des universités et des organismes gouvernementaux connus sous le nom de ZigBee
    Alliance
    ```  
  
    Le truc de cool, c'est qu'il à beaucoup de grande marque qui ont adopté ce protocole, voir la [liste de plus de 600 appareils qui parlent ce protocole](https://www.zigbee2mqtt.io/information/supported_devices.html) ! dont *Ikea*, *Philips*, *Xiaomi* etc...  
    
    ### Le protocole ZigBee comparé aux autres : 
    ![communication protocole comparaison](img/protocol_range.jpg)  
    
- Le protocole de *messagerie* **mqtt** (MQ Telemetry Transport):  

    - Pour plus d'explications sur le net:  
        - Une [petite video](https://www.youtube.com/watch?v=7skzc4axLKM) explicative en français  
        - [ici, un article de blog explicatif](https://www.lebigdata.fr/mqtt-tout-savoir)  
         
         
    ```
    MQTT a été inventé en 1999 par des chercheurs d’IBM qui cherchaient à avoir un protocole de 
    messagerie léger pour faire communiquer des machines dans un environnement où les déconnexions
    sont fréquentes.  
      
    Contrairement au principe du client/serveur utilisé sur le Web, MQTT utilise celui de la
    publication/souscription : plusieurs clients se connectent à un serveur unique (appelé broker)
    pour soit publier des informations, soit souscrire à leur réception.
    ```  
    
- Le programme **Node-Red**:  

    ```
    Node-Red est un serveur Web consommant peu de ressources, capable de fonctionner sur
    un Raspberry Pi qui permet très rapidement de concevoir et déployer des scénarios d'automatisation dans sa maison.  
    Il permet en quelques clics de créer des scénario plus puissants que toutes les box domotiques du marché.
    Toutes les fonctions / connecteurs mis à disposition de l'outil sont adaptables facilement, il est aussi possible
    de créer ses propres modules, bref c'est un régal !
    ``` 

    - Une serie de tutos vidéo:  
            - [partie 1](https://www.youtube.com/watch?v=OhwY5ly6uxY)  
            - [partie 2](https://www.youtube.com/watch?v=3Sx8PbbNX-Q)  
            - [partie 3](https://www.youtube.com/watch?v=D6ndgxrPwq0)  

    - Une liste de vidéo illustrant l'utilisation de Node-Red
        - https://blogmotion.fr/diy/node-red-raspberry-pi-16516

## Ce que va permettre de faire le setup du notre projet sur le Raspberry-Pi:  

Pour pouvoir controller une installation domotique avec des appareils qui parlent le ZigBee (ampoule, capteur de précense, capteur de température, prise connecté, etc...), Il faudra au préalable acheter un HUB de la marque choisie (boitier électronique qui va permettre communiquer avec les objets). Mais aussi, installer et utiliser l'application mobile de la même marque pour controller ce petit monde.  

Le **dongle USB CC2531** (sniffer zigbee) en conjonction avec le programme **ZigBee2mqtt** va nous permettre de communiquer simplement avec tous les objects connectés zigbee, avec le protocole de messagerie trés utilisé **mqtt**. Le dongle va donc:  
  
- Remplacer un hub propriétaire  
- Permettre d'utiliser dans une solution domotique, plusieurs marques differentes d'objets connectés.  
- Controller ce petit monde avec un troisième programme [Node-Red](https://nodered.org/)  (programme Node.js) qui permet de créer ultra facilement des *scénarios domotique* (entre-autre). Ce programme est une WebApp qui tournera dans un navigateur depuis notre Raspberry-Pi.  

Le tout en utilisant le programme simple et visuel [Node-Red](https://nodered.org/)


**Nous avons donc, installé sur ton Rapberry-Pi Zero et qui tournent en permanence:**  
  
- Le programme **ZigBee2mqtt**, qui écoute et transmet des messages de tes objets connecté *ZigBee*.
 
- Un serveur de messagerie *mqtt* (c'est la version *[mosquitto](https://projetsdiy.fr/mosquitto-broker-mqtt-raspberry-pi/)*) qui sert de point central pour l'échanges de message entres tes objets connectés (*zigbee* ou autres ! )  
    > port par default :  1883

- La WebApplication *node-red* pour controller & écouté tes objets connectés.
    pour ouvrir l'application depuis un navigateur:
    > port par defaut : 1880  
    et donc, ouvrir un navigateur et RDV au : http://[ADRESS IP DU RASPBERRY]:1880  


## Usage  

Le point d'entrée est l'application **node-red**, pour rappel c'est une webApplication (application qui tourne sur un navigateur web).  
Pour y acceder:  
> http://[ADRESS IP DU RASPBERRY]:1880  

L'application *node-red* peut controller énormément de type de d'objet connecté, pas seulement ceux avec le protocole *ZigBee* mais aussi ceux wifi comme des ampoules, des webccam, des prises connectés, etc... Il suffit de rajouter à l'application le pluggin/module qui va bien ([voir tuto pour rajouter un module](https://projetsdiy.fr/node-red-installer-desinstaller-modules/)).  
  
Dans notre cas, nous allons voir comment gérer des objects avec le protocole de communication *ZigBee* en utilisant le protocole de messagerie *mqtt*.

### Se connecter au RaspberryPi en SSH  

> `$ ssh pi@[ IP DU RASPBERRY ] -p 22`  
 login : pi  
 mdp : ***   
  
### Appairer un objet *zigbee*  

Par défault, le fichier de configuration du programme *zigbee2mqtt* autorise l'appairage de nouveaux objets, vérifier que la valuer `permit_join: true` est à `true` du fichier `configuration.yaml`  
> chemin du fichier configuration.yaml :  
/opt/zigbee2mqtt/data/configuration.yaml

- La procédure d'apperrage propre à chaque object est décrite dans la [liste des objets compatibles](https://www.zigbee2mqtt.io/information/supported_devices.html) sur le site du dongle *zigbee2mqtt* (il suffit de cliquer sur le lien html, dans la première colonne "model", la procédure spécifique sera décrite).  

### Vérifier que le nouvel objet est appairé:  
Une fois appairé, le nouveau device va se rajouter dans la liste `devices` du fichier `configuration.yaml`. ci dessous la liste de 2 devices déjà appairés dans le salon de la maison de St-Marcel:  
```  
  devices:  
     '0x14b457fffec8d85a':  
        friendly_name: 'ikea-switch'  
     '0x00158d00039fa219':  
        friendly_name: 'xiaomi-plug'  
```
Par défault, la propriété `friendly_name` à la valeur de l'id de l'objet, il convient alors de modifier cette valuer pour pouvoir utiliser de façon adequat le protocole de messagerie *mqtt*, voir ce petit tuto sur [les conventions](https://www.hivemq.com/blog/mqtt-essentials-part-5-mqtt-topics-best-practices/) pour exploiter pleinement ce protocole de messagerie.  
      
>**IMPORTANT : Aprés modification de la propriété *friendly_name* il faut re-demarrer le programme zigbee2mqtt pour appliquer les modifs:**    
  `$ sudo systemctl restart zigbee2mqtt`  

- **IMPORTANT**
    Par défaut, le topic de base (message de base) que génère le programme zigbee2mqtt est `zigbee2mqtt`  
    
### Gérer les ojects avec *mqtt*  
Voir l'unique exemple dans *node-red*.  
  
Le "flow" unique, écoute le message emit par l'interrupteur `ikea-switch`, et en fonction de la valuer de la propriéte `click` affecte la valeur `state` a la prise Xiaomi `xiaomi-plug`.  

Double cliquer sur le node nommé  `interrupteur` pour voir le petit bout de code qui fait la condition.  
      
> Il existe énormement de ressources et de tutoriels pour utiliser le programme node-red qui est tres puissant, difficile de faire un résumé simple dans cette petite fiche de préesntation du projet.  

    
    
## SETUP  

### Flash du dongle CC2531  

- [installer Wiring-Pi](http://wiringpi.com/download-and-install/)  
- installer git (si pas déjà fait)  
    `sudo apt-get install git`  
- check avec la commande `$ gpio -v` si tout va bien !
  
```  
Raspberry-Pi	            Dongle CC2531
GND                             GDN
Pin 38	                        DD
Pin 36	                        DC
Pin 35	                        RST
```

RaspberryPi                |   Dongle CC2531 
:-------------------------:|:-------------------------:
![connexion rpi](img/RPI-pins.png)  |  ![fil connexion rpi](img/CC2531.jpg)  

- Cloner les ressources nécessaires:  
    `git clone https://github.com/jmichault/flash_cc2531.git`
- Aller dans le dossier *flash_cc2531* et faire un petit check
    `./cc_chipid`  : doit  être `ID = b524.`  
- Erase  
    `./cc_erase`
- Flash avec le bon fichier (dossier ressources, deplacer le fichier dans le dossier *flash_cc2531*):  
    `./cc_write CC2531ZNP-Prod.hex`  
    
    
### Setup logiciel
- [installer Node.js](http://wiringpi.com/download-and-install/)  
- [Installer node-red](https://nodered.org/docs/getting-started/raspberrypi)    
    > RDV:  
     [IP DU RASPBERRY PI]:1880  
- [Installer & lancer ZigBee2MQTT](https://www.zigbee2mqtt.io/getting_started/running_zigbee2mqtt.html)  
    - Commandes utiles  
    > *Status !*  
    `sudo systemctl status zigbee2mqtt.service`

    > *Stopping zigbee2mqtt*  
    sudo systemctl stop zigbee2mqtt
    
    > *Starting zigbee2mqtt &  re-starting*  
    sudo systemctl start zigbee2mqtt
    sudo systemctl restart zigbee2mqtt

    > *View the log of zigbee2mqtt*  
    sudo journalctl -u zigbee2mqtt.service -f

- [Installer Mosquitto MQTT Brocker](https://randomnerdtutorials.com/how-to-install-mosquitto-broker-on-raspberry-pi/)  
    > Port par default : 1883      
    
    - Commandes utiles  
        > *status !*  
        `sudo systemctl status mosquitto.service`
      
## Ressources  

- Le tuto complet:     
    - video:    
        https://notenoughtech.com/home-automation/flashing-cc2531-without-cc-debugger/  
    - blog:  
        https://notenoughtech.com/home-automation/flashing-cc2531-without-cc-debugger/
        
    
- Pour Flasher la clef, blog avec plus de détails pour le fichier à utiliser:  
     - https://lemariva.com/blog/2019/07/zigbee-flashing-cc2531-using-raspberry-pi-without-cc-debugger  
     
 






